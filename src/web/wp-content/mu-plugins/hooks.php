<?php
/**
 * Must Use hooks
 *
 * @package WordPress
 */

/* Disable XML-RPC */
add_filter( 'xmlrpc_enabled', '__return_false' );

/* Hide pingback URL */
add_filter(
	'wp_headers',
	function( $headers ) {
		unset( $headers['X-Pingback'] );
		return $headers;
	}
);

/* Remove default emoji resource hints */
add_filter(
	'wp_resource_hints',
	function( $hints, $relation_type ) {
		foreach( $hints as $k => $v ) {
			if ( strpos( $v, '//s.w.org/' ) ) {
				unset( $hints[$k] );
			}
		}
		return $hints;
	},
	10,
	2
);

/*
if ( defined( 'WP_AUTO_UPDATE_CORE' ) && WP_AUTO_UPDATE_CORE && WP_AUTO_UPDATE_CORE != 'minor' ) {
	// Enable automatic updates for all plugins.
	add_filter( 'auto_update_plugin', '__return_true' );

	// Enable automatic updates for all themes.
	add_filter( 'auto_update_theme', '__return_true' );
}
*/

/* Disable writes to .htaccess file */
/* add_filter( 'flush_rewrite_rules_hard','__return_false' ); */

/* Do not log IP address for comments */
add_filter( 'pre_comment_user_ip', '__return_empty_string' );

/* Clean up HTML <head> */
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'start_post_rel_link' );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'adjacent_posts_rel_link' );

/* Clean up WordPress backend */
if ( is_admin() ) {
	add_action(
		'admin_menu',
		function() {
			/* Hide admin menu items */
			//remove_menu_page( 'index.php' ); // Dashboard.
			//remove_menu_page( 'edit.php' ); // Posts.
			//remove_menu_page( 'upload.php' ); // Media.
			//remove_menu_page( 'edit.php?post_type=page' ); // Pages.
			//remove_menu_page( 'edit-comments.php' ); // Comments.
			//remove_menu_page( 'themes.php' ); // Appearance.
			//remove_menu_page( 'plugins.php' ); // Plugins.
			//remove_menu_page( 'users.php' ); // Users.
			//remove_menu_page( 'tools.php' ); // Tools.
			//remove_menu_page( 'options-general.php' ); // Settings.
			//remove_menu_page( 'edit.php?post_type=custom_post_type' ); // Custom post type.

			/* Remove news from dashboard */
			remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );
		}
	);

	/* Remove welcome panel from dashboard */
	remove_action( 'welcome_panel', 'wp_welcome_panel' );
}

/* Use UUID for GUID */
add_filter( 'wp_insert_post_data', function ( $data, $postarr ) {
	if ( '' === $data['guid'] ) {
		$data['guid'] = wp_slash( 'urn:uuid:' . wp_generate_uuid4() );
	}

	return $data;
}, 11, 2 );

/* Remove JQuery migrate */
add_action( 'wp_default_scripts', function ( $scripts ) {
	$scripts->remove( 'jquery-migrate' );
	$scripts->add( 'jquery-migrate', null );
}, PHP_INT_MAX );

/* Sharpen thumbnail images */
add_filter(
	'image_make_intermediate_size',
	function( $file ) {
		$image = wp_load_image( $file );
		if ( !is_resource( $image ) ) {
			return new WP_Error( 'error_loading_image', $image, $file );
		}
	
		$size = getimagesize( $file );
		if ( ! isset( $size[2] ) ) {
			return new WP_Error( 'invalid_image', __( 'Could not read image size' ), $file );
		}

		if ( IMAGETYPE_JPEG === $size[2] ) {
			$matrix = array(
				array( -1, -1, -1 ),
				array( -1, 16, -1 ),
				array( -1, -1, -1 ),
			);
	
			$divisor = array_sum( array_map( 'array_sum', $matrix ) );
			$offset = 0; 
			imageconvolution( $image, $matrix, $divisor, $offset );
			imagejpeg( $image, $file, apply_filters( 'jpeg_quality', 90, 'edit_image' ) );
		}

		return $file;
	},
	99
);
